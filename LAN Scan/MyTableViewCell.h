//
//  MyTableViewCell.h
//  LAN Scan
//
//  Created by Андрей on 30.06.2018.
//  Copyright © 2018 Andrey Fokin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddr;


@end

NS_ASSUME_NONNULL_END
